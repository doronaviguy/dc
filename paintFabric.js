﻿
///**********************************//
/// <reference path="main.js" />
/// <reference path="paintFabric.js" />
/// <reference path="proxy.js" />
///*********************************//

var Paint = (function ($) {
  //var baseUrl = "http://dcserver.doron.c9.io"
  var baseUrl = "http://localhost:8000";
  var _domCntnr = null;
  var _cnvs = null;
  var _prgrsBar = null;
  var _lastAction = "FreeDraw";
  var _layer = null;
  var _isInit = false;
  var _isTBInit = false;
  var _imgId = 0;
  var _checkIfDoneHndl = -1;
  var _isVisilbe = false;
  var count = 0;
  var _imgToWrap;
  var TEXT_INITAL_WIDTH = 0.95; /* 80% of canvas width*/
  var PENCIL_WIDTH = '15';

  ////////////////////////////////////////////////////////////////////////////////////////////
  var _init = function (cnvsId, imgToWrap) {

    cnvsId = cnvsId || "canvas";
    imgToWrap = imgToWrap || _imgToWrap;
    /* private */
    if (_isInit) {
      //_startIsDoneTimer();
      $('#dcwrapper').show();
      return;
    }

    var getVetricalAlign = function (domEL) {
      var prntTop = domEL.parent().height();
      var itTop = domEL.height();
      return (prntTop / 2) - (itTop / 2);
    };

    isInit = true;
    var $Xbtn = $("<div id='cnvsClsBtn' class='blueBtn'></div>");
    $Xbtn.click(function () {
      console.log("closing ! 1 ! !!! ");
      _hide();
    });

    $('#dcwrapper').append('<div id="dcTxtElCntnr"> <input  id="dcTxtEl"  type="text"/><button id="btnSavTxt" class="paintItm plain" clbk="SaveText" >Enter</button></div>');
    $('#dcwrapper').append('<div id="progressBar"></div>');
    _prgrsBar = $("#progressBar");
   //$("#dcTxtEl").width($('#dcTxtElCntnr').width());


    //_initCornerTB();
    //$(".prev , .next").hide();ij
    _cnvs = new fabric.Canvas(cnvsId);
    
    _cnvs.freeDrawingBrush.color = "#ccc";
    _cnvs.freeDrawingBrush.width = 10;
    _cnvs.freeDrawingBrush.shadowBlur = 2;

    _setImgToWrap(imgToWrap);
    $('#dcwrapper').append($Xbtn);


    $(window).on('resize',_cnvsResizeHndlr);
    $(window).trigger('resize');

    _cnvs.freeDrawingLineWidth = PENCIL_WIDTH;

    //pathGallery.init($("#dcwrapper"));

    var imgId = document.location.href.match(/fbid=([^&]*)/);
    if (imgId && imgId.length > 1)
      imgId = imgId[1];
    DC.imgId = imgId;
    //_startIsDoneTimer();

  };
  ////////////////////////////////////////////////////////////////////////////////////////////
  var _showProgressBar = function (isDon) {
    if (!isDon)
      _prgrsBar.show();
    else
      _prgrsBar.hide();
  }
  ////////////////////////////////////////////////////////////////////////////////////////////
  var _cnvsResizeHndlr = function (e) {
    console.log('==>window resize ! ');
    var dcwrapper = $('#dcwrapper'),
    height = _imgToWrap.height(),
    width = _imgToWrap.width();

    _cnvs.setHeight(height);
    _cnvs.setWidth(width);

    _prgrsBar.css({ height: height, width: width });



    var it = _imgToWrap;
    if (it) /*theater mode*/{
      var spotlight = it.find('img.spotlight').length == 1 ? it.find('img.spotlight') : it;

      dcwrapper.css({ top: spotlight.offset().top, left: it.offset().left });
      dcwrapper.width(spotlight.width());
      dcwrapper.height(height);
    }

    var oldW = parseInt(dcwrapper.attr("oldWidth")),
    oldH = parseInt(dcwrapper.attr("oldHeight")),
    crntWidth = dcwrapper.width(),
    crntHeight = dcwrapper.height();
    if (oldW == crntWidth && oldH == crntHeight)
      return;

    alphaW = crntWidth / oldW;
    alphaH = crntHeight / oldH;

    console.log('==>alphas ' + alphaW + " :" + alphaH);
    dcwrapper.attr("oldWidth", crntWidth);
    dcwrapper.attr("oldHeight", crntHeight);

    setTimeout(function () {
      _scale(alphaW, alphaH);
    }, 50);
  }
  ////////////////////////////////////////////////////////////////////////////////////////////
  var _setImgToWrap = function (/*$*/img) {
    if (_imgToWrap == img)
      return;
    _imgToWrap = img;
    $(window).trigger('resize');
  }
  ////////////////////////////////////////////////////////////////////////////////////////////
  var _initCornerTBisInit = false;
  var _initCornerTB = function () {
    if (_initCornerTBisInit)
      return;
    var el = $("<div class='cronerTB'/>").html("<div id='savePaintBtn' class='blueBtn'>Save</div><div id='clearPaintBtn' class='blueBtn'>X</div>");



    $('#dcwrapper').append(el);
    $("#clearPaintBtn").click(function () {
      _action({ type: "Clear" });
    });
    $("#savePaintBtn").click(function () {
      _action({ type: "Json" });
    });

    _initCornerTBisInit = true;
  }
  ////////////////////////////////////////////////////////////////////////////////////////////
  var _startIsDoneTimer = function () {
    checkIfDoneHndl = setInterval(_checkIfDone, 10000);
  };
  var _checkIfDone = function () {
    console.log('==>checkIfDone is called()');
    if (document.location.pathname.indexOf("/photo.php") == -1) {
      console.log('==>checkIfDone ture  Hiding !');
      //$('#dcwrapper').hide();
      clearInterval(checkIfDoneHndl);
    }

  };
  ////////////////////////////////////////////////////////////////////////////////////////////
  var _initToolBox = function (/*$ object*/wrapper) {
    if (_isTBInit)
      return true;

    _isTBInit = true;


    var $paintTB = $("<div id='paintCntnr'><div id='paintItm-wrapper'></div></div>");
    var paintBtns = [];
    //    paintBtns.push($("<button class='paintItm rect' clbk='Rect' />"));
    //    paintBtns.push($("<button class='paintItm cicrle' clbk='Circle' />"));
    paintBtns.push($("<button class='paintItm txt' clbk='Text' />"));
    paintBtns.push($("<button class='paintItm pencil selected' clbk='FreeDraw' />"));
    //paintBtns.push($("<button class='paintItm heart'        clbk='Effect' />"));
    //paintBtns.push($("<button class='paintItm kiss'         clbk='Kiss' />"));
    //paintBtns.push($("<button class='paintItm callout-left' clbk='CallOutLeft' />"));
    //paintBtns.push($("<button class='paintItm callout-right' clbk='CallOutRight' />"));
    //paintBtns.push($("<button class='paintItm save' clbk='Json' />"));
    paintBtns.push($("<div id='color-pickerFill' ></div>"));
    paintBtns.push($("<div style='height:12px;' ></div>"));
    paintBtns.push($("<button class='paintItm plain btns-ok-cncl' clbk='Clear' >Clear</button>"));
    paintBtns.push($("<button class='paintItm plain btns-ok-cncl' clbk='Json' >Done</button>"));
    //paintBtns.push($("<div id='color-pickerStroke' ></div>"));





    for (var i = 0; i < paintBtns.length; i++) {
      $(paintBtns[i]).click(function (e) {

        console.log($(this).attr('clbk'));
        //    e.preventDefault();

        $(this).parent().find('.selected').removeClass('selected');
        $(this).addClass('selected');

        _action({ type: $(this).attr('clbk') });
      });
      $paintTB.find('#paintItm-wrapper').append(paintBtns[i]); //append to #paintCntnr
    }

    wrapper.append($paintTB);
    $("#color-pickerFill").colorpicker({
      color: '408EDC',
      invertControls: false,
      controlStyle: 'inset',
      swatches: false,
      realtime: true,

      onSelect: function (color, inst) {

        //activeEl = _cnvs.getActiveObject();
        

        //activeEl.set(activeEl.type == 'path' ? 'stroke' : 'fill', color.rgba);
        _cnvs.freeDrawingBrush.color = $("#color-pickerFill").colorpicker("option", "color").hex;
        //_action(_lastAction || "FreeDraw");
      }
    });
    _bindHndlrs();
    _isInit = true;
  };
  ////////////////////////////////////////////////////////////////////////////////////////////
  var _bindHndlrs = function () {

    _cnvs.observe('mouse:up', function (e) {
      window.getSelection().empty(); // clear seleciton
      console.log('=*******************=> mouseup .....');
      //e.preventDefualt();
      return false;
    });

    _cnvs.observe("path:created", function (e) {
      
      var curnt = e.path;
      curnt.setShadow({ color: 'rgba(0,0,0,0.8)', offsetX:5, offsetY:5 , blur:5 });
      _redraw();
    });

    $(document).keydown(function (e) {

      var keycode = e.keyCode;
      console.log('==>keycode = ' + keycode)
      if (keycode == 46) /*delete*/ {
        var curnt = _cnvs.getActiveObject();
        _cnvs.remove(curnt);
      }
      if (37 <= keycode && keycode <= 40) {

        var alpha = -1, curnt = null;

        if (!(keycode % 2))
          alpha = keycode == 38 ? 'top:-10' : 'top:10';
        else
          alpha = keycode == 37 ? 'left:-10' : 'left:10';


        curnt = _cnvs.getActiveObject();
        if (alpha == -1 || !curnt)
          return;
        alpha = alpha.split(':');
        var prop = alpha[0], val = alpha[1];

        val = parseInt(val) + parseInt(curnt.get(prop));
        console.log('==>key navigation new val is = ' + val);
        curnt.set(prop, val);

        _redraw();
      }
    });



  };
  ////////////////////////////////////////////////////////////////////////////////////////////
  var _action = function (data) {


    switch (data.type) {
      case "Rect":
        _addRect();
        break;
      case "Circle":
        _addCircle();
        break;
      case "Text":
        _addText(data.text);
        break;
      case "FreeDraw":
        _freeDraw();
        break;
      case "Effect":
        _effect();
      case "Kiss":
      case "CallOutLeft":
      case "CallOutRight":
      case "Heart":
        _drawSVG(data.type);
        break;
      case "Json":
        _toJson();
        break;
      case "Clear":
        _clear();
        break;
      case "SaveText":
        _saveText();
        break;
    }
    if (data.type != "FreeDraw" && data.type != "Clear") {
      _cnvs.isDrawingMode = false;
      _cnvs.CURSOR = 'crosshair';
      //$("#paintItm-wrapper .selected").removeClass("selected");
    }


    _lastAction = data.type;

    _cnvs.calcOffset(); /* very important */
    _redraw();

  };
  ////////////////////////////////////////////////////////////////////////////////////////////

  var _svgMap = {

    Kiss: "kiss",
    Heart: "heart",
    CallOutLeft: "callout_left",
    CallOutRight: "callout_right"

  }

  var _drawSVG = function (name) {

    var svgName = _svgMap[name];
    if (!svgName)
      return;
    fabric.Image.fromURL( baseUrl +'/svg/' + svgName + '.svg', function (img) {
      _cnvs.add(img.set({ left: 250 + (count++ * 5), top: 350 + (count++ * 5), angle: 0 }));
    });

  }
  ////////////////////////////////////////////////////////////////////////////////////////////
  var convrtVal = function (prop, val) {
    return parseFloat(prop, 10) * val;
  };
  ////////////////////////////////////////////////////////////////////////////////////////////
  var _scaleLock = 0;
  var _scale = function (alphaW, alphaH, /*func*/done) {

   console.log('==>enter _scale() w =' + alphaW + " h =" + alphaH);

    var now = new Date();
    if (false && _scaleLock && (now - _scaleLock) < 1000) {
      if (done) done();
      return;
    }
    _scaleLock = now;

    if (alphaH == 1 && alphaW == 1 || (isNaN(alphaH) && isNaN(alphaW))){
      if (done) done();
      return;
    }

    $('canvas').css({ 'opacity': 0 });
    //console.log('==>enter _scale() w =' + alphaW + " h =" + alphaH);

    var height = _cnvs.getHeight(),
    width = _cnvs.getWidth();

    _cnvs.forEachObject(function (it) {
      if (!it)
        return;

      var currentObjTop = it.get('top'),
            currentObjLeft = it.get('left'),
            currentObjScaleX = it.get('scaleX'),
            currentObjScaleY = it.get('scaleY'),
            scaledObjTop = convrtVal(currentObjTop, alphaH),
            scaledObjLeft = convrtVal(currentObjLeft, alphaW),
            scaledObjScaleX = convrtVal(currentObjScaleX, alphaW),
            scaledObjScaleY = convrtVal(currentObjScaleY, alphaH);

      it.set({
        top: scaledObjTop,
        left: scaledObjLeft,
        scaleX: scaledObjScaleX,
        scaleY: scaledObjScaleY
      });
      it.setCoords();
    });

    _redraw();

    _cnvs.scaleValueH = alphaH;
    _cnvs.scaleValueW = alphaW;

    $('canvas').animate({ 'opacity': 1 });
    if (done)
      done();
    
    

  };
  ////////////////////////////////////////////////////////////////////////////////////////////
  var _addRect = function () {

    var rect = new fabric.Rect({
      left: 100 + (count++ * 5),
      top: 100 + (count++ * 5),
      width: 100,
      height: 100,
      fill: 'green'
    });
    //_bindShapeEvents(circle, layer);
    _cnvs.add(rect);
  };
  var _addCircle = function () {
    var circle = new fabric.Circle({
      left: 100 + (count++ * 5),
      top: 100 + (count++ * 5),
      radius: 10,
      fill: 'red',
      stroke: 'blue'
    });
    //_bindShapeEvents(rect, layer);
    _cnvs.add(circle);
  };
  ////////////////////////////////////////////////////////////////////////////////////////////
  var _effect = function () {
    var theaterImg = $('div.stage img.spotlight');
    if (!theaterImg.length) {
      console.log("no theater Image");
      return;
    }
    var srcimg = theaterImg.attr("src");
    var size = {};
    size.height = theaterImg.height();
    size.width = theaterImg.width();
    _showProgressBar(false);
    BgProxy.makeRqst(
        { type: 'getImgAsDataUrl', src: srcimg, effect:"lomo", size: size },
        function (msg) {
          console.log('==>dataUrl is [length] =  ' + msg.data.length);      

          var img = fabric.document.createElement('img');
          console.log(msg.data);
          img.src = msg.data;
          var image = new fabric.Image(img);
          image.scaleToHeight(size.height);
          image.set({
           
            left: _cnvs.getWidth() / 2,
            top: _cnvs.getHeight() / 2,
            selectable: false
          });
          
          
         
          _cnvs.add(image);
          setTimeout(function () {
            _showProgressBar(true);
            _redraw();
          }, 2);
    }); 
  }
  ////////////////////////////////////////////////////////////////////////////////////////////
  var _addText = function (text) {
   

    var textObj = new fabric.Text('some text ', {
      fontFamily: 'Delicious',
      left: _cnvs.getWidth() / 2 + (count++ * 15),
      top: _cnvs.getHeight() / 3 + (count++ * 15),
      fill: $("#color-pickerFill").colorpicker("option", "color").hex,
      textShadow: 'rgba(0,0,0,0.6) 3px 3px 3px'

    });
    //    var textSample = new fabric.Text( text, {
    //      left: getRandomInt(100, 350),
    //      top: getRandomInt(320, 220),
    //      fontFamily: 'Delicious_500',
    //      fill: '#' + "ccc",
    //      scaleX:1,
    //      scaleY: 1
    //    });

    _cnvs.observe('object:selected', function (e) {

      var curnt = _cnvs.getActiveObject();
      
      if (curnt.type == 'text') {
        $("#dcTxtElCntnr").show();
        var txtEl = $("#dcTxtEl");
        txtEl.val(curnt.getText()).focus();

        txtEl.on('keyup', function () {
          
          curnt = _cnvs.getActiveObject();
          if (curnt.type == 'text') {
            curnt.setText(txtEl.val());
            console.log("text keyup.....");
            _redraw();
          }
        });

      }
      else {
        $("#dcTxtElCntnr").hide();
        $("#dcTxtEl").off('change');
      }
    });

    _cnvs.add(textObj).setActiveObject(textObj);
    _cnvs.isDrawingMode = false;
    $("#dcTxtEl").focus();
    _redraw();
  };
  ////////////////////////////////////////////////////////////////////////////////////////////
  var _freeDraw = function () {
    _cnvs.freeDrawingBrush.color = $("#color-pickerFill").colorpicker("option", "color").hex;
    _cnvs.isDrawingMode = !_cnvs.isDrawingMode;
    $("#dcTxtElCntnr").hide();
    if (_cnvs.isDrawingMode)
      $("canvas").addClass('custom-cursor');
    else
      $("canvas").removeClass('custom-cursor');

    _cnvs.CURSOR = chrome.extension.getURL('img/cursor-pencil.png');
  };

  ////////////////////////////////////////////////////////////////////////////////////////////
  var _redraw = function () {

    if (_cnvs.isDrawingMode)
      _cnvs.CURSOR = chrome.extension.getURL('img/cursor-pencil.png');
    else
      _cnvs.CURSOR = 'crosshair';
     

    _cnvs.renderAll();
  };
  ////////////////////////////////////////////////////////////////////////////////////////////
  var showCmntTxtPu = function () {
    var $deferd = $.Deferred();
    
    var pu = $("#cmntpuDOM");
    if (!pu.length) {

      $("#dcwrapper").append($(["<div id='cmntpuDOM'>",
                                 "<span class='putitle'>Please enter comment body</span>",
                                 "<textarea id='cmntbody' ></textarea>",
                                 "<br> <input type='button' class='paintItm plain' value='Post Comment' id='cmntsubmit' />",
                                 "<input type='button' class='paintItm plain' value='Cancel' id='cmntcncl' />",
                                 "</div>"].join("")
                                 ));
      pu = $("#cmntpuDOM");
      $('div.stage img.spotlight').addClass("blurd");

    } else {
      $('#cmntbody').val('');
    }
    pu.show();
    $('div.stage img.spotlight').addClass("blurd");


    $("#cmntsubmit").click(function () {
      var val = $('#cmntbody').val();
      if (val) {
        $('div.stage img.spotlight').removeClass("blurd");
        $deferd.resolve(val); /// will call the post comment clbk Promise Pattern
        pu.hide();
       
      } else {
        $("#cmntbody").attr('placeholder', 'please add comment text');
      }
    });

    $("#cmntcncl").click(function () {
      pu.hide();
      $('div.stage img.spotlight').removeClass("blurd");
    });

    return $deferd;
  }
  ////////////////////////////////////////////////////////////////////////////////////////////
  var _toJsonLock = null;
  var _toJson = function () {
    console.log("toJson objects count is = " + _cnvs.toJSON().objects.length);
    //var rslt = _cnvs.toJSON();
    var d1 = new Date();
    if (_toJsonLock && (d1 - _toJsonLock) < 1000)
      return;
    _toJsonLock = d1;

    wrap = $('#dcwrapper');
    var cnvsData = JSON.stringify(_cnvs);
    var cnvsData = "{\"size\":{\"height\":" + wrap.height() + ", \"width\":" + wrap.width() + "}, " + cnvsData.substring(1);
    
    //_showProgressBar(false);

    DC.getImgId(document.location.ref);
    
    showCmntTxtPu().then(function (cmntTxt) {
      _showProgressBar(false);
      setTimeout(function () { _showProgressBar(true); }, 7000);

      BgProxy.makeRqst({ type: 'uploadDrawing', postId: DC.imgId, data: cnvsData, txt: cmntTxt },
          function (msg) {
            //    _showProgressBar(true);
            _showProgressBar(true);
            console.log('==>new comment id is =<  ' + msg);
            $(window).trigger("theaterMode");
            setTimeout(function () {
              $(window).trigger("theaterMode");
            }, 1000);
          });
    });
  };
  ////////////////////////////////////////////////////////////////////////////////////////////
  var _showCmntPp = function() {
    var defer = $.Deferred();


    return defer;
  }

  ////////////////////////////////////////////////////////////////////////////////////////////
  var _oldJson = null;
  var _jsonCache = {};
  var _loadJson = function (json) {

    var jsonSize = { width: 1, height: 1 };
    _init();

    if (_oldJson == json || !json) {
      console.log("load json faild (oldjson is equal");
      return;
    }
    
    $('canvas').css({ 'opacity': 0 });
    
    jsonSize = json.size || jsonSize;
    _oldJson = json;


    _cnvs.loadFromDatalessJSON(json, function () {
      var dcwrapper = $('#dcwrapper'),
      aplhaW = dcwrapper.width() / jsonSize.width;
      alphaH = dcwrapper.height() / jsonSize.height;

      _scale(aplhaW, alphaH, function() {
        $('canvas').animate({ 'opacity': 1 }, "fast");
      });
      
      
    });

  };
  ////////////////////////////////////////////////////////////////////////////////////////////
  var _readOnlyMode = function (isEnd/**/) {
    isEnd = !!isEnd; //defualt start
   
    if (!isEnd) {
      $("#paintCntnr").hide();
      $('.upper-canvas').hide();
    } else {
      $("#paintCntnr").show();
      $('.upper-canvas').show();
      _cnvs.selection = true;
    }
  };
  ////////////////////////////////////////////////////////////////////////////////////////////
  var _saveText = function () {
    var textObj = _cnvs.getActiveObject();
    textObj.setText(txtEl.val());
    _redraw();
  }
  ////////////////////////////////////////////////////////////////////////////////////////////
  var _clear = function () {
    if (_cnvs && _cnvs.clear)
      _cnvs.clear();

    $("#cmntpuDOM").hide();
  };
  ////////////////////////////////////////////////////////////////////////////////////////////
  var _hideVisual = function () {
    $('#dcwrapper').hide();
  };
  ////////////////////////////////////////////////////////////////////////////////////////////
  var _hide = function () {
      
    if ($("#dcwrapper").is(":visible")) {
       
        console.log("dcwrapper is visible and Paint.hide() called");
        $('#dcwrapper').hide(function () {
          _clear();
          $("#cmntpuDOM").hide();
        });
      }
    //$("#dcwrapper").fadeOut(5000);
  };

  ////////////////////////////////////////
  return {
    init: _init,
    setCnvsPrnt :_setImgToWrap,
    initToolBox: _initToolBox,
    action: _action,
    loadJson: _loadJson,
    readOnlyMode: _readOnlyMode,
    clear: _clear,
    hide: _hide,
    hideVisual: _hideVisual
  }
  /////////////////////////////////////////
})(jQuery);


function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}


//////////////// https://graph.facebook.com/10150847633225807/ ccomments


