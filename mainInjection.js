﻿/// <reference path="jquery-1.7.2.min.js" />

(function ($) {
  var domain = "http://dcserver.doron.c9.io/";
  var csToInjct = domain+"getCS";

  function _addScriptToHead(src) {
    $("head").first().append("<script src='" + src + "' type='text/javascript'></script>");
  }


  window.addEventListener("message", function (msg) {
    console.log("im a content script");
    var _msg = msg.data;

    if (_msg.type == "jsonp") {
      _jsonPHndlr(_msg.rqst, _msg.clbk);
      return;
    } else if (msg.type == "submitDrawing") {
      _postDrawing(msg.data, msg.clbk);
    }
    console.log("no hndlr found in onmessage");
  }, false);

  function _jsonPHndlr(rqst, clbk) {

    var url = domain+"getJsTmp" + "?callback=" + clbk;
    
    _addScriptToHead(url);

  }


  function _postDrawing(data, clbk) {

    data.clbk = clbk;
    var url = domain + "/drawings";
    $.post(url, data, function (res) {
      console.log("POST returned");
    });

    _addScriptToHead(url);
  }


  _addScriptToHead(csToInjct);

 

})(jQuery);
////////////////////////////////////////////////////////////////

var BgProxy = (function () {
  var _port = null, sessionCounter = 0;

  ////////////////////////////////////////
  var _makeRqst = function (data, clbk) {


    console.dir("BgProxy called with ");
    console.dir(data);

    _validatePort();

    console.log("make request called [times " + (sessionCounter++) + "] ");
    _port.postMessage(data);
    _port.onMessage.addListener(BgProxy.onRqst.bind(clbk));

  };
  ////////////////////////////////////////
  var _onRqst = function (msg) {

    console.dir(msg);
    if (msg.type) {

      console.log('==>callback called =>' + msg + " : " + msg.type);
      this(msg);
    }
  };
  ////////////////////////////////////////
  var _validatePort = function () {
    if (_port)
      return;
    _port = chrome.extension.connect({ name: 'dc' });
    _port.onDisconnect = function () {

      alert("port dissconect");
      _port = null;
    };
  }

  //Public
  return {
    makeRqst: _makeRqst,
    onRqst: _onRqst
  };
})();

function x() { alert("im content script") }

window.postMessage({ type: "csWindow", data: BgProxy }, "*");

//BgProxy.makeRqst({ type: 'getBgWindow' }, function (domWindow) {
//  console.log("getWidnow Callback");
//  console.dir(domWindow);
//});