﻿


localStorage.removeItem('dclogin'); /////////////////todo remove ...


chrome.extension.onConnect.addListener(function (port) {

  console.log('==>msg  chrome.extension.onConnect  ')
  port.onMessage.addListener(function (msg) {
    CsListener.onmessage(port, msg);
  });

});


var CsListener = (function () {

  ///////////////////////////////////////////////
  var _msgHndlr = function (port, msg) {


    console.log('==>onmessage called | type = ' + msg.type);
    switch (msg.type) {

      case 'getImgsByPostId':
        DcAjx.getImgsByPostId(msg, port);
        break;
      case 'uploadDrawing':
        DcAjx.uploadDraw(msg, port);
        break;
      case 'getDrawJson':
        DcAjx.getDrawJson(msg, port);
      case 'getImgAsDataUrl':
        DcAjx.imgToBase64(msg, port);
    }
  };
  var _clbkHndlr = function (port, data) {

    port.postMessage(data);

  };
  ///////////////////////////////////////////////
  return {
    onmessage: _msgHndlr,
    messageClbk: _clbkHndlr
  };
})();

var DcAjx = (function () {




  //var _baseUrl = 'http://dcapi.jit.su/';
  //var _baseUrl = "http://dcserver.doron.c9.io/"
  //var _baseUrl = "http://dcapi.eu01.aws.af.cm/"
  //var _baseUrl = 'http://localhost/';
  var _baseUrl = 'http://drawcomment.com/';
  //var _baseUrl = 'http://dcapix.jit.su/';

  var _baseStorageUrl = 'https://s3.amazonaws.com/dcdrawing/';
  var NO_AUTH_MSG = "noAuth";

  var _tokenTimeOut = 60;
  ///////////////////////////////////////////////
  var login = function (clbkAction) {


    if (localStorage.dclogin) {

      var d1 = new Date(localStorage['dclogin']), d2 = new Date();

      if ((d2 - d1) / 1000 / 60 < _tokenTimeOut) {
        console.log('not expierd ' + (d2 - 1) / 1000 / 60)
        return true;
      }
    }
    $.get(_baseUrl + 'auth/facebook', null, function (rslt) {
      console.log(rslt);
      var d = new Date();
      localStorage['dclogin'] = d;
      clbkAction();
    });
    return false;

  };
  ///////////////////////////////////////////////

  var uploadDrawTrys = 0;
  var uploadDraw = function (imgData/* {postId : int, data: js} */, port) {

    console.log("upload draw");
    console.dir(imgData);

    if (login(function () { uploadDraw(imgData, port); })) {
      var form = {};

      form['postId'] = imgData.postId;
      form['data'] = imgData.data;
      form['cmntTxt'] = imgData.txt;

      $.post(_baseUrl + "drawings/", form, function (newPostId) {
        console.log('drawings upload returned [newPostId] =>' + newPostId);

        if (newPostId == "error-try-again") {
          login(function () { uploadDraw(imgData, port); });
          return;
        } else if(newPostId =="error") {
          ////////////// error  falid to post , and probably to login
          console.log("newPostId (probably bad oath flow" + newPostId);
          return;
        }

        


        if (newPostId == NO_AUTH_MSG) {
          console.log('No auth trying to Login again try #' + uploadDrawTrys);

          uploadDrawTrys++;
          localStorage.dclogin = null;

          if (uploadDrawTrys > 5) {
            console.log('uploadDrawTrys reached limit [Canceling operation]');
            uploadDrawTrys = 0;
            return;
          }
          return login(function () { uploadDraw(imgData, port); });

        }

        console.log("uploadImg Xhr done newPostId = " + newPostId);
        var msg = { type: 'uploadDrawing', data: newPostId };
        CsListener.messageClbk(port, msg);
      });

    }
  };
  ///////////////////////////////////////////////
  var getDrawJson = function (msgData/* {jsonUrl : int, data: js} */, port) {

    console.log('getDrawJson IN [msgData.jsonUr] = ' + msgData.jsonUrl);

    $.getJSON(msgData.jsonUrl, null, function (data) {
      console.log('data = ' + JSON.stringify(data));
      var msg = { type: 'getDrawJson', data: data };
      CsListener.messageClbk(port, msg);
    });


  }

  /*  */
  ///////////////////////////////////////////////
  var getImgsByPostId = function (imgData /* {postId : int} */, port) {


    if (login(function () { getImgsByPostId(imgData, port) })) {

      $.get(_baseUrl + 'drawings/' + imgData.postId,
      function (filesList /*{ path,files:[] } */) {
        var msg = { type: 'getImgsByPostId', data: filesList};
        CsListener.messageClbk(port, msg);
      });

    }
  };
  ///////////////////////////////////////////////
  var canvas = document.createElement("canvas");
	
  


  
  function imgToBase64(msg, port) {
    ctx = canvas.getContext('2d'); 
    console.log("current canvas data => " + canvas.toDataURL());
    var img = document.createElement("img");
    canvas.id = "cnvs";
    var size = msg.size;
    document.body.appendChild(canvas);
    var effect = msg.effect;
    var src = msg.src;
    console.log("src = "+src);
    if (!src) {
      console.log('imagetobase64 faild');
    }
    img.style.height = size.height;
    img.style.width = size.width;
    img.onload = function () {
      canvas.height = size.height;
      canvas.width = size.width;
      ctx.drawImage(img, 0, 0);

      Caman("#cnvs", function () {
        Caman.Event.listen("renderFinished", function (job) {
          setTimeout(function () {
            var data = canvas.toDataURL("image/png");
            console.log(data);
            var msg = { type: 'imgToBase64', data: data };
            CsListener.messageClbk(port, msg);
            releaseCnvs();
          }, 20);
        });

        this["lomo"]();
        this.render();
      });
      delete img;
    }
    img.src = src;


  };

  function releaseCnvs() {
    
    var ctx = canvas.getContext('2d');
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    console.log("canvas was released \n data.length =" + canvas.toDataURL());
    img = null;
  }
  ///////////////////////////////////////////////
  return {
    uploadDraw: uploadDraw,
    getDrawJson: getDrawJson,
    getImgsByPostId: getImgsByPostId,
    imgToBase64: imgToBase64
  };
})();


//login();




